﻿using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailGunWrapper
{
    public class MailGunHelper
    {
        private readonly string _emailValdiationResouce = "address/validate";
        private const string EmailSendingResource = "sandbox56c8d564bf464aca900cc0c283a37b20.mailgun.org/messages";
        private const string ValidationKey = "pubkey-3ad4651ed760e9b4d42aef65962d40b6";

        private readonly RestClient _client = new RestClient()
        {
            BaseUrl = new Uri("https://api.mailgun.net/v3"),
            Authenticator = new HttpBasicAuthenticator("api",
                                            "key-dd6173ed1ec5689420b377d641166c73")
        };



        public SendMailResponse SendMail(MailGunEmailModel email)
        {
            var request = new MailGunEmailRequest {Resource = EmailSendingResource};

            //Build parameters Up
            GenerateEmailRequestParameters(email, request);
            
            //Make Request
            var deserializer = new JsonDeserializer();
            var result = deserializer.Deserialize<SendMailResponse>(_client.Execute(request));
            return result;



        }
        /// <summary>
        /// Parses the email and adds the required parameters to the request
        /// </summary>
        /// <param name="email">Email  which came from outside of the wrapper</param>
        /// <param name="request"></param>
        private void GenerateEmailRequestParameters(MailGunEmailModel email, MailGunEmailRequest request)
        {
            if (email == null) throw new ArgumentNullException(nameof(email));

            foreach (var to in email.To)
            {
                request.AddParameter("to", to);
            }

            foreach (var from in email.From)
            {
                request.AddParameter("from", from);
            }


            if (!string.IsNullOrEmpty((email.Text)))
                request.AddParameter("text", (email.Text));

            if (!string.IsNullOrEmpty(email.Html))
                request.AddParameter("html", (email.Html));


            request.AddParameter("o:tag", email.Tag);
            request.AddParameter("o:campaign", email.CampaignId);
            request.AddParameter("o:testmode", ParseMailGunBool(email.IsTestingMode));
            request.AddParameter("o:tracking", ParseMailGunBool(email.ShouldTrackEmail));
            request.AddParameter("o:tracking-clicks", ParseMailGunBool(email.ShouldTrackClicks));
            request.AddParameter("o:tracking-opens", ParseMailGunBool(email.ShouldTrackOpens));
        }

        private static string ParseMailGunBool(bool mailGunParameter)
        {
            var result = mailGunParameter ? "yes" : "no";
            return result;
        }

        public MailGunEmailValidationResult ValidateAddress(string address)
        {
           
            _client.Authenticator =
                new HttpBasicAuthenticator("api",
                                            ValidationKey);
            var request = new RestRequest {Resource = _emailValdiationResouce};
            request.AddParameter("address", address);
            var deserializer = new JsonDeserializer();
            var requestresult = _client.Execute(request);
            return deserializer.Deserialize<MailGunEmailValidationResult>(requestresult);
        
        
           
        }


    }

  

  


    
}
