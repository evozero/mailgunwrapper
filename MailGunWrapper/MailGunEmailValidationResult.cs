﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailGunWrapper
{
    public class Parts
    {
        public object display_name { get; set; }
        public string local_part { get; set; }
        public string domain { get; set; }
    }

    public class MailGunEmailValidationResult
    {
        public bool is_valid { get; set; }
        public string address { get; set; }
        public Parts parts { get; set; }
        public object did_you_mean { get; set; }
    }

}
