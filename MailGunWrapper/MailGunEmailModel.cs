﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailGunWrapper
{

    public class MailGunEmailModel
    {

        public IEnumerable<string> From { get; set; }
        public IEnumerable<string> To { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
        public string Html { get; set; }
        /// <summary>
        /// Id of the campaign the message belongs to. See um-campaign-analytics for details.
        /// </summary>
        public int? CampaignId { get; set; }
        /// <summary>
        /// Tag string. 
        /// </summary>
        public string Tag { get; set; }

        public bool IsTestingMode { get; set; } = true;
        /// <summary>
        /// 	Toggles tracking on a per-message basis, see Tracking Messages for details. Pass yes or no.
        /// </summary>
        public bool ShouldTrackEmail { get; set; } = false;
        /// <summary>
        /// Toggles clicks tracking on a per-message basis. Has higher priority than domain-level setting. Pass yes, no or htmlonly.
        /// </summary>
        public bool ShouldTrackClicks { get; set; } = false;
        /// <summary>
        /// 	Toggles opens tracking on a per-message basis. Has higher priority than domain-level setting. Pass yes or no.
        /// </summary>
        public bool ShouldTrackOpens { get; set; } = false;


    }
}
