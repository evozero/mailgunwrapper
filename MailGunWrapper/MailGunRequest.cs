﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailGunWrapper
{
    
    public class MailGunEmailRequest : RestRequest
    {
        public MailGunEmailRequest() : base()
        {
            this.AddParameter("domain", "YOUR_DOMAIN_NAME", ParameterType.UrlSegment);
            this.Method = Method.POST;
        }
    }

}
