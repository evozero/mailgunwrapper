﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailGunWrapper
{
    public class SendMailResponse
    {
        public string message { get; set; }
        public string id { get; set; }
    }

}
