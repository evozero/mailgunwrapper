﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Shouldly; 

namespace MailGunWrapper.Tests
{
    [TestClass()]
    public class MailGunHelperTests
    {
        [TestMethod()]
        public void SendMail_TestMode()
        {
            //Arrange
            var wrapper = new MailGunHelper();


            var email = new MailGunEmailModel
            {
                From = new List<string>() { "nholder88@gmail.com" },
                To = new List<string>() { "nholder88@gmail.com" },
                Subject = "Testing",
                Html = @"<h1>Testing</h1>",
                IsTestingMode = true,
                Text = "This is the main test",
                Tag = "Test"
            };
            //Act 
            var result = wrapper.SendMail(email);
            //Assert
            result.id.ShouldNotBeNullOrEmpty();
        }
        [TestMethod]
        public void SendMail_TestMpde_Off()
        {
            //Arrange
            var wrapper = new MailGunHelper();


            var email = new MailGunEmailModel
            {
                From = new List<string>() { "nholder88@gmail.com" },
                To = new List<string>() { "nholder88@gmail.com" },
                Subject = "Testing",
                Html = @"<h1>Testing</h1>",
                IsTestingMode = false,
                ShouldTrackClicks = true,
                ShouldTrackEmail = true,
                ShouldTrackOpens = true,
                Text = "This is the main test",
                Tag = "Test"
            };
            //Act 
            var result = wrapper.SendMail(email);
            //Assert
            result.id.ShouldNotBeNullOrEmpty();
        }

        [TestMethod()]
        public void ValidateAddress_InvalidEmail()
        {
            var wrapper = new MailGunHelper();

            var result= wrapper.ValidateAddress("fdsgdgrgerger");

            result.is_valid.ShouldBe(false);
        }


        [TestMethod()]
        public void ValidateAddress_ValidEmail()
        {
            var wrapper = new MailGunHelper();
            var result = wrapper.ValidateAddress("nholder88@gmail.com");
            result.is_valid.ShouldBe(true);
        }
    }
}